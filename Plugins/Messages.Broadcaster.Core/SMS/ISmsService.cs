﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nopforest.Plugin.Messages.Broadcaster.Core.SMS
{
    public interface ISmsService
    {
        Task<string> SendMessage(string username, string password, string source, IEnumerable<string> destination, string message, TimeSpan? sendtime = null, object parameters = null);
        string Name { get; }
        string Description { get; }
        string Website { get; }
    }
}
