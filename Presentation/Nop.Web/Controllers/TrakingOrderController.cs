﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Infrastructure;
using Nop.Services.Configuration;
using Nop.Services.ExportImport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nop.Web.Controllers
{
    public class TrakingOrderController : BasePublicController
    {
        private readonly INopFileProvider _fileProvider;
        private readonly ISettingService _settingService;
        private readonly IStaticCacheManager _staticCacheManager;

        public TrakingOrderController(INopFileProvider fileProvider,
            ISettingService settingService, IStaticCacheManager staticCacheManager)
        {
            _fileProvider = fileProvider;
            _settingService = settingService;
            _staticCacheManager = staticCacheManager;
        }
        [HttpGet, Route(@"~/GetTrakingNo/")]

        public ActionResult GetTrakingNo()
        {
            return View();
        }

        [HttpPost, Route(@"~/GetTrakingNo/{mobile}")]
        public ActionResult GetTrakingNo(string mobile)
        {
            if (string.IsNullOrEmpty(mobile))
                return Json(new List<TrakingOrder>());

            mobile = mobile.TrimStart('0');
            var path = _fileProvider.MapPath(ImportManager.TrakingPath);
            var lastMount = DateTime.Now.AddDays(_settingService.GetSettingByKey("Order.TrakingDays", -30));
            var files = _staticCacheManager.Get("OrderTraking.TrakingFiles", () =>
            {
                if (_fileProvider.DirectoryExists(path))
                {
                    return System.IO.Directory.GetFiles(path, "*.json").Select(x => new FileInfo(x)).Select(x => (FileInfo: x, DateTime: long.Parse(Regex.Match(x.Name, @"\d+").Value ?? "0"))).OrderByDescending(x => x.DateTime).Where(x => x.DateTime > lastMount.ToFileTime());
                }
                return null;
            });
            if (files != null)
            {
                var history = _staticCacheManager.Get("OrderTraking.History", () =>
                {
                    var list = new List<TrakingOrder>();
                    foreach (var item in files)
                    {
                        if (item.FileInfo.Exists)
                            list.AddRange(JsonConvert.DeserializeObject<List<TrakingOrder>>(System.IO.File.ReadAllText(item.FileInfo.FullName)));
                    }
                    return list;
                });
                return Json(history.Where(x => x.Mobile.Contains(mobile))
                    .Select(x =>
                    {
                        return new { x.FirstName, x.LastName, x.TrakingNo, x.Date , x.Destination};
                    }).OrderByDescending(x => x.Date));
            }
            return null;
        }
    }
}
