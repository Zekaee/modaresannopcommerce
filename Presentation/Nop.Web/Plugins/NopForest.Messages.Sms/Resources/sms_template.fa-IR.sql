﻿DELETE [dbo].[LocalizedProperty] WHERE [LocaleKeyGroup] = 'SmsTemplate'
TRUNCATE TABLE [NopForest].[SmsTemplate]
DECLARE @TempFa  TABLE ([Name] NVARCHAR(MAX), [Body] NVARCHAR(MAX), [IsActive] BIT, [DelayBeforeSend] BIT, [SmsAccountId] INT, [LimitedToStores] BIT, [DelayPeriod] INT)
DECLARE  @TempEn  TABLE ([Name] NVARCHAR(MAX), [Body] NVARCHAR(MAX), [IsActive] BIT, [DelayBeforeSend] BIT, [SmsAccountId] INT, [LimitedToStores] BIT, [DelayPeriod] INT)
INSERT @TempFa VALUES ( N'Blog.BlogComment', N'%Store.Name% %Store.URL%''  يک نظر جديد براي پست وبلاگ ''%BlogComment.BlogPostTitle%'' ثبت شده است.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.BackInStock', N'%Store.Name% %Store.URL%''  %Customer.FullName%، با سلام  %BackInStockSubscription.ProductName% %BackInStockSubscription.ProductUrl%  در انبار موجود شد.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.EmailValidationMessage', N'%Store.Name% %Store.URL%''  براي فعال شدن حساب خود اينجا  %Customer.AccountActivationURL% را کليک کنيد.  %Store.Name%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.EmailRevalidationMessage', N' %Store.Name% %Store.URL%''  %Customer.FullName%! باسلام   براي اعتبارسنجي ايميل جديد اينجا  %Customer.EmailRevalidationURL% را کليک کنيد.   %Store.Name%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.NewPM', N' %Store.Name% %Store.URL%''  شما يک پيام خصوصي دريافت کرده‌ايد.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.PasswordRecovery', N'%Store.Name% %Store.URL%''  براي تغيير گذرواژه خود اينجا %Customer.PasswordRecoveryURL% را کليک کنيد. %Store.Name%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.WelcomeMessage', N'به فروشگاه %Store.Name% %Store.URL%'' خوش آمديد.   اکنون مي‌توانيد از خدمات مختلف فروشگاه ما بهره‌مند شويد.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Forums.NewForumPost', N' %Store.Name% %Store.URL%''   پست جديدي در تاپيک ''%Forums.TopicName%'' %Forums.TopicURL%'' در انجمن ''%Forums.ForumName%'' %Forums.ForumURL%'' ايجاد شده است.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Forums.NewForumTopic', N' %Store.Name% %Store.URL%''  تاپيک ''%Forums.TopicName%'' %Forums.TopicURL%'' در انجمن ''%Forums.ForumName%'' %Forums.ForumURL%'' ايجاد شد.  براي اطلاعات بيشتر اينجا  %Forums.TopicURL% را کليک کنيد.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'GiftCard.Notification', N' يک کارت هديه از %Store.Name% دريافت کرده‌ايد    %GiftCard.RecipientName% عزيز,  کارت هديه‌اي براي فروشگاه %Store.Name% %Store.URL%'' براي شما فرستاده شده است.   کد کارت هديه شما %GiftCard.CouponCode% است  %GiftCard.Message%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewCustomer.Notification', N' %Store.Name% %Store.URL%''   مشتري جديد در فروشگاه شما ثبت‌نام کرده است. جزئيات:   نام:%Customer.FullName%   ايميل:%Customer.Email% N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewReturnRequest.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName% درخواست مرجوعي جديد را ثبت کرد. جزئيات:    شماره درخواست:%ReturnRequest.CustomNumber%   محصول:%ReturnRequest.Product.Quantity%x محصول:%ReturnRequest.Product.Name%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewReturnRequest.CustomerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%! با سلام   شما يک درخواست مرجوعي کالا ثبت کرده‌ايد. جزئيات:   شناسه درخواست:%ReturnRequest.CustomNumber%   محصول:%ReturnRequest.Product.Quantity%x محصول:%ReturnRequest.Product.Name%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'News.NewsComment', N' %Store.Name% %Store.URL%''  نظر جديدي براي خبر با عنوان ''%NewsComment.NewsTitle%'' ثبت شد.N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewsLetterSubscription.ActivationMessage', N' براي تأييد اشتراک خود در ليست ما اينجا را کليک کنيد %NewsLetterSubscription.ActivationUrl%''', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewsLetterSubscription.DeactivationMessage', N' براي لغو اشتراک از خبرنامه ما اينجا را کليک کنيد. %NewsLetterSubscription.DeactivationUrl%''N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'NewVATSubmitted.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%(%Customer.Email%) يک شماره ماليات بر ارزش افزوده جديد ارسال کرده است. جزئيات:   شماره ماليات بر ارزش افزوده:%Customer.VatNumber%   وضعيت ماليات بر ارزش افزوده:%Customer.VatNumberStatus%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderCancelled.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName% با سلام   سفارش شما لغو شده است.  جزئيات: <a target =''_blank'' href=''%Order.OrderURLForCustomer%''>%Order.OrderURLForCustomer%</a>N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderCompleted.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%، با سلام  سفارش شما تکميل شده است. جزئيات:  شماره سفارش:%Order.OrderNumber%   <a target =''_blank'' href=''%Order.OrderURLForCustomer%''>%Order.OrderURLForCustomer%</a>N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'ShipmentDelivered.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%، با سلام  سفارش شما تحويل داده شد.   شماره سفارش:%Order.OrderNumber%   %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%'' target=''_blankN', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPlaced.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%، با سلام   بابت خريد از فروشگاه %Store.Name%</a> از شما متشکريم.  شماره سفارش:%Order.OrderNumber%   <a target =''_blank'' href=''%Order.OrderURLForCustomer%''>%Order.OrderURLForCustomer% %Store.URL%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPlaced.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%(%Order.CustomerEmail%) سفارشي در فرشگاه شما ثبت کرده است.   شماره سفارش:%Order.OrderNumber%  N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'ShipmentSent.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%! با سلام  سفارش شما حمل شده است.   شماره سفارش:%Order.OrderNumber%   %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%'' target=''_blankN', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Product.ProductReview', N' %Store.Name% %Store.URL%''  يک نظر جديد براي ''%ProductReview.ProductName%'' ثبت شده است.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'QuantityBelow.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  موجودي %Product.Name%(ID:%Product.ID%)  کم است.   تعداد:%Product.StockQuantity%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'QuantityBelow.AttributeCombination.StoreOwnerNotification', N' %Store.Name% %Store.URL%''   موجودي %Product.Name%(ID:%Product.ID%) کم است.   %AttributeCombination.Formatted%   تعداد:%AttributeCombination.StockQuantity%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'ReturnRequestStatusChanged.CustomerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%، با سلام   وضعيت درخواست مرجوعي شما #%ReturnRequest.CustomNumber% تغيير کرده است.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Service.EmailAFriend', N' %Store.Name% %Store.URL%''   %EmailAFriend.Email% از فروشگاه %Store.Name%  خريد کرده است و تمايل دارد مواردي را با شما به اشتراک گذارد. <a target =''_blank'' href=''%Product.ProductURLForCustomer%''>%Product.Name%</a>  براي اطلاعات بيشتر <a target =''_blank'' href=''%Product.ProductURLForCustomer%''>اينجا</a>  را کليک کنيد.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Wishlist.EmailAFriend', N' %Store.Name% %Store.URL%''   %Wishlist.Email%از فروشگاه %Store.Name%  خريد کرده است و تمايل دارد ليست علاقه‌مندي‌هاي خود را با شما به اشتراک گذارد.   براي اطلاعات بيشتر <a target =''_blank'' href=''%Wishlist.URLForCustomer%''>اينجا</a> را کليک کنيد.   %Store.Name% N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Customer.NewOrderNote', N' %Store.Name% %Store.URL%''  %Customer.FullName%، با سلام   يادداشت سفارش جديد به حساب شما افزوده شد.   ''%Order.NewNoteText%''.   <a target =''_blank'' href=''%Order.OrderURLForCustomer%''>%Order.OrderURLForCustomer%</a> N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'RecurringPaymentCancelled.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  %if (%RecurringPayment.CancelAfterFailedPayment%) آخرين پرداخت براي تکرار پرداخت با شناسه %RecurringPayment.ID% ناموفق بود. endif%%if (!%RecurringPayment.CancelAfterFailedPayment%)%Customer.FullName%(%Customer.Email%) پرداخت تکراري با شناسه %RecurringPayment.ID% را لغو کرده است.endif% N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'RecurringPaymentCancelled.CustomerNotification', N' %Store.Name% %Store.URL%''   %Customer.FullName%، با سلام   %if (%RecurringPayment.CancelAfterFailedPayment%) به نظر مي‌رسد که اين پرداخت با کارت اعتباري شما،مقدور نيست. (%Order.OrderURLForCustomer% %Order.OrderURLForCustomer%'' target=''_blank'')  بنابراين اشتراک شما لغو شده است.endif%%if (!%RecurringPayment.CancelAfterFailedPayment%) پرداخت با شناسه =%RecurringPayment.ID%لغو شد.endif% N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'RecurringPaymentFailed.CustomerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%، با سلام   به نظر مي‌رسد که اين پرداخت با کارت اعتباري شما،مقدور نيست (%Order.OrderURLForCustomer% %Order.OrderURLForCustomer%'' target=''_blank'')  %if (%RecurringPayment.RecurringPaymentType%== ''Manual'')  پس از شارژ مجدد مي‌توانيد پرداخت را انجام دهيد و يا در صفحه تاريخچه سفارشات، آن را لغو کنيد. endif%%if (%RecurringPayment.RecurringPaymentType%== ''Automatic'')  پس از شارژ مجدد، منتظر بمانيد. سعي خواهيم کرد پرداخت را مجدداً انجام دهيم. يا مي‌توانيد سفارش را در صفحه تاريخچه سفارش لغو کنيد. endif% N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPlaced.VendorNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%(%Customer.Email%) سفارشي را ثبت کرده است.  شماره سفارش:%Order.OrderNumber%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderRefunded.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%، باسلام   با تشکر از خريد شما از فروشگاه %Store.Name%</a> . سفارش #%Order.OrderNumber% لغو شده است. لطفاً 7-14 روز براي بازگشت مبلغ به حسابتان، صبر کنيد.  مبلغ مرجوعي شده:%Order.AmountRefunded%  شماره سفارش:%Order.OrderNumber%   <a href =''%Order.OrderURLForCustomer%'' target=''_blank''>%Order.OrderURLForCustomer% %Store.URL%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderRefunded.StoreOwnerNotification', N'%Store.Name%. لغو سفارش #%Order.OrderNumber%  سفارش #%Order.OrderNumber% لغو شده است.  مبلغ مرجوعي:%Order.AmountRefunded%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPaid.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  سفارش #%Order.OrderNumber% پرداخت شده است.', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPaid.CustomerNotification', N' %Store.Name% %Store.URL%''  %Order.CustomerFullName%، با سلام   با تشکر از خريد شما از %Store.Name%</a>. سفارش #%Order.OrderNumber% پرداخت شده است. جزئيات :  شماره سفارش:%Order.OrderNumber%    <a href =''%Order.OrderURLForCustomer%'' target=''_blank''>%Order.OrderURLForCustomer% %Store.URL%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'OrderPaid.VendorNotification', N' %Store.Name% %Store.URL%''  سفارش #%Order.OrderNumber% پرداخت شد.  شماره سفارش:%Order.OrderNumber%', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'VendorAccountApply.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  %Customer.FullName%(%Customer.Email%) براي حساب فروشنده درخواست داده است.جزئيات:   نذم فروشنده:%Vendor.Name%   ايميل:%Vendor.Email%N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'VendorInformationChange.StoreOwnerNotification', N' %Store.Name% %Store.URL%''  فروشنده با نام %Vendor.Name%(%Vendor.Email%)، اطلاعات خود را تغيير داده است. N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Service.ContactUs', N' %ContactUs.Body%  N', 1, 0, 1, 0, 0)
INSERT @TempFa VALUES ( N'Service.ContactVendor', N' %ContactUs.Body%  N', 1, 0, 1, 0, 0)





INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Blog.BlogComment', '
%Store.Name% %Store.URL%
 
 
A new blog comment has been created for blog post "%BlogComment.BlogPostTitle%".

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('QuantityBelow.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Product.Name% (ID: %Product.ID%) low quantity.
 
 
Quantity: %Product.StockQuantity%
 

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('QuantityBelow.AttributeCombination.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Product.Name% (ID: %Product.ID%) low quantity.
 
%AttributeCombination.Formatted%
 
Quantity: %AttributeCombination.StockQuantity%
 

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('ReturnRequestStatusChanged.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
Your return request #%ReturnRequest.CustomNumber% status has been changed.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Service.EmailAFriend', '
 %Store.Name% %Store.URL%
 
 
%EmailAFriend.Email% was shopping on %Store.Name% and wanted to share the following item with you.
 
 
<b>%Product.Name% %Product.ProductURLForCustomer%</b>
 
%Product.ShortDescription%
 
 
For more info click here %Product.ProductURLForCustomer%
 
 
 
%EmailAFriend.PersonalMessage%
 
 
%Store.Name%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Wishlist.EmailAFriend', '
 %Store.Name% %Store.URL%
 
 
%Wishlist.Email% was shopping on %Store.Name% and wanted to share a wishlist with you.
 
 
 
For more info click here %Wishlist.URLForCustomer%
 
 
 
%Wishlist.PersonalMessage%
 
 
%Store.Name%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.NewOrderNote', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
New order note has been added to your account:
 
"%Order.NewNoteText%".
 
%Order.OrderURLForCustomer% %Order.OrderURLForCustomer%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('RecurringPaymentCancelled.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%if (%RecurringPayment.CancelAfterFailedPayment%) The last payment for the recurring payment ID=%RecurringPayment.ID% failed, so it was cancelled. endif% %if (!%RecurringPayment.CancelAfterFailedPayment%) %Customer.FullName% (%Customer.Email%) has just cancelled a recurring payment ID=%RecurringPayment.ID%. endif%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('RecurringPaymentCancelled.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
%if (%RecurringPayment.CancelAfterFailedPayment%) It appears your credit card didn''t go through for this recurring payment (%Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank)
 
So your subscription has been canceled. endif% %if (!%RecurringPayment.CancelAfterFailedPayment%) The recurring payment ID=%RecurringPayment.ID% was cancelled. endif%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('RecurringPaymentFailed.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
It appears your credit card didn''t go through for this recurring payment (%Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank)
  %if (%RecurringPayment.RecurringPaymentType% == "Manual") 
You can recharge balance and manually retry payment or cancel it on the order history page. endif% %if (%RecurringPayment.RecurringPaymentType% == "Automatic") 
You can recharge balance and wait, we will try to make the payment again, or you can cancel it on the order history page. endif%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPlaced.VendorNotification', '
%Store.Name% %Store.URL%
 
 
%Customer.FullName% (%Customer.Email%) has just placed an order.
 
 
Order Number: %Order.OrderNumber%
 
Date Ordered: %Order.CreatedOn%
 
 
%Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPlaced.AffiliateNotification', '
%Store.Name% %Store.URL%
 
 
%Customer.FullName% (%Customer.Email%) has just placed an order.
 
 
Order Number: %Order.OrderNumber%
 
Date Ordered: %Order.CreatedOn%
 
 
%Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderRefunded.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Thanks for buying from %Store.Name% %Store.URL%. Order #%Order.OrderNumber% has been has been refunded. Please allow 7-14 days for the refund to be reflected in your account.
 
 
Amount refunded: %Order.AmountRefunded%
 
 
Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
<br /
>Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderRefunded.StoreOwnerNotification', '%Store.Name%. Order #%Order.OrderNumber% refunded'', N''

%Store.Name% %Store.URL%
 
 
Order #%Order.OrderNumber% has been just refunded
 
 
Amount refunded: %Order.AmountRefunded%
 
 
Date Ordered: %Order.CreatedOn%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPaid.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
Order #%Order.OrderNumber% has been just paid
 
Date Ordered: %Order.CreatedOn%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPaid.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Thanks for buying from %Store.Name% %Store.URL%. Order #%Order.OrderNumber% has been just paid. Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPaid.VendorNotification', '
%Store.Name% %Store.URL%
 
 
Order #%Order.OrderNumber% has been just paid.
 
 
Order Number: %Order.OrderNumber%
 
Date Ordered: %Order.CreatedOn%
 
 
%Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPaid.AffiliateNotification', '
%Store.Name% %Store.URL%
 
 
Order #%Order.OrderNumber% has been just paid.
 
 
Order Number: %Order.OrderNumber%
 
Date Ordered: %Order.CreatedOn%
 
 
%Order.Product(s)%

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('VendorAccountApply.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Customer.FullName% (%Customer.Email%) has just submitted for a vendor account. Details are below:
 
Vendor name: %Vendor.Name%
 
Vendor email: %Vendor.Email%
 
 
You can activate it in admin area.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('VendorInformationChange.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
Vendor %Vendor.Name% (%Vendor.Email%) has just changed information about itself.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('ProductReview.Reply.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
You received a reply from the store administration to your review for product "%ProductReview.ProductName%".

', 0, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Product.ProductReview', '
%Store.Name% %Store.URL%
 
 
A new product review has been written for product "%ProductReview.ProductName%".

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('ShipmentSent.CustomerNotification', '
 %Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%!,
 
Good news! You order has been shipped.
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% Shipped Products:
 
 
%Shipment.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPlaced.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Order.CustomerFullName% (%Order.CustomerEmail%) has just placed an order from your store. Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.BackInStock', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%,
 
Product %BackInStockSubscription.ProductName% %BackInStockSubscription.ProductUrl% is in stock.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.EmailValidationMessage', '%Store.Name% %Store.URL%
 
 
To activate your account click here %Customer.AccountActivationURL%.
 
 
%Store.Name%
', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.EmailRevalidationMessage', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%!
 
To validate your new email address click here %Customer.EmailRevalidationURL%.
 
 
%Store.Name%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.NewPM', '
%Store.Name% %Store.URL%
 
 
You have received a new private message.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.PasswordRecovery', '%Store.Name% %Store.URL%
 
 
To change your password click here %Customer.PasswordRecoveryURL%.
 
 
%Store.Name%
', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Customer.WelcomeMessage', 'We welcome you to  %Store.Name% %Store.URL%.
 
 
You can now take part in the various services we have to offer you. Some of these services include:
 
 
Permanent Cart - Any products added to your online cart remain there until you remove them, or check them out.
 
Address Book - We can now deliver your products to another address other than yours! This is perfect to send birthday gifts direct to the birthday-person themselves.
 
Order History - View your history of purchases that you have made with us.
 
Products Reviews - Share your opinions on products with our other customers.
 
 
For help with any of our online services, please email the store-owner: %Store.Email% mailto:%Store.Email%.
 
 
Note: This email address was provided on our registration page. If you own the email and did not register on our site, please send an email to %Store.Email% mailto:%Store.Email%.
', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Forums.NewForumPost', '
%Store.Name% %Store.URL%
 
 
A new post has been created in the topic "%Forums.ForumName%" %Forums.ForumURL% forum.
 
 
Click here %Forums.TopicURL% for more info.
 
 
Post author: %Forums.PostAuthor%
 
Post body: %Forums.PostBody%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Forums.NewForumTopic', '
%Store.Name% %Store.URL%
 
 
A new topic "%Forums.ForumName%" %Forums.ForumURL% forum.
 
 
Click here %Forums.TopicURL% for more info.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('GiftCard.Notification', '
You have received a gift card for %Store.Name%


Dear %GiftCard.RecipientName%,
 
 
%GiftCard.SenderName% (%GiftCard.SenderEmail%) has sent you a %GiftCard.Amount% gift cart for  %Store.Name% %Store.URL%


You gift card code is %GiftCard.CouponCode%


%GiftCard.Message%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Service.ContactUs', '
%ContactUs.Body%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewCustomer.Notification', '
%Store.Name% %Store.URL%
 
 
A new customer registered with your store. Below are the customer''s details:
 
Full name: %Customer.FullName%
 
Email: %Customer.Email%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewReturnRequest.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Customer.FullName%!
 
You have just submitted a new return request. Details are below:
 
Request ID: %ReturnRequest.CustomNumber%
 
Product: %ReturnRequest.Product.Quantity% x Product: %ReturnRequest.Product.Name%
 
Reason for return: %ReturnRequest.Reason%
 
Requested action: %ReturnRequest.RequestedAction%
 
Customer comments:
 
%ReturnRequest.CustomerComment%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('News.NewsComment', '
%Store.Name% %Store.URL%
 
 
A new news comment has been created for news "%NewsComment.NewsTitle%".

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewsLetterSubscription.ActivationMessage', '
Click here to confirm your subscription to our list. %NewsLetterSubscription.ActivationUrl%


If you received this email by mistake, simply delete it.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewsLetterSubscription.DeactivationMessage', '
Click here to unsubscribe from our newsletter. %NewsLetterSubscription.DeactivationUrl%


If you received this email by mistake, simply delete it.

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewVATSubmitted.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Customer.FullName% (%Customer.Email%) has just submitted a new VAT number. Details are below:
 
VAT number: %Customer.VatNumber%
 
VAT number status: %Customer.VatNumberStatus%
 
Received name: %VatValidationResult.Name%
 
Received address: %VatValidationResult.Address%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderCancelled.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Your order has been cancelled. Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderCompleted.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Your order has been completed. Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('ShipmentDelivered.CustomerNotification', '
 %Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Good news! You order has been delivered.
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%" target="_blank
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% Delivered Products:
 
 
%Shipment.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('OrderPlaced.CustomerNotification', '
%Store.Name% %Store.URL%
 
 
Hello %Order.CustomerFullName%,
 
Thanks for buying from %Store.Name% %Store.URL%. Below is the summary of the order.
 
 
Order Number: %Order.OrderNumber%
 
Order Details: %Order.OrderURLForCustomer% %Order.OrderURLForCustomer%
 
Date Ordered: %Order.CreatedOn%
 
 
 
 
Billing Address
 
%Order.BillingFirstName% %Order.BillingLastName%
 
%Order.BillingAddress1%
 
%Order.BillingCity% %Order.BillingZipPostalCode%
 
%Order.BillingStateProvince% %Order.BillingCountry%
 
 
 
 
%if (%Order.Shippable%) Shipping Address
 
%Order.ShippingFirstName% %Order.ShippingLastName%
 
%Order.ShippingAddress1%
 
%Order.ShippingCity% %Order.ShippingZipPostalCode%
 
%Order.ShippingStateProvince% %Order.ShippingCountry%
 
 
Shipping Method: %Order.ShippingMethod%
 
 
 endif% %Order.Product(s)%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('NewReturnRequest.StoreOwnerNotification', '
%Store.Name% %Store.URL%
 
 
%Customer.FullName% has just submitted a new return request. Details are below:
 
Request ID: %ReturnRequest.CustomNumber%
 
Product: %ReturnRequest.Product.Quantity% x Product: %ReturnRequest.Product.Name%
 
Reason for return: %ReturnRequest.Reason%
 
Requested action: %ReturnRequest.RequestedAction%
 
Customer comments:
 
%ReturnRequest.CustomerComment%

', 1, 0 , 1, 0, 0)
INSERT INTO @TempEn(
      [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] ) VALUES('Service.ContactVendor', '
%ContactUs.Body%

', 1, 0 , 1, 0, 0)

INSERT [Nopforest].[SmsTemplate] SELECT [Name]
      ,[Body]
      ,[IsActive]
      ,[DelayBeforeSend]
      ,[SmsAccountId]
      ,[LimitedToStores]
      ,[DelayPeriod] FROM  @TempEn

DECLARE @LangId INT
SELECT  @LangId  = Id FROM [dbo].[Language] WHERE [UniqueSeoCode] = 'fa'


INSERT INTO [dbo].[LocalizedProperty]
	SELECT Id, @LangId ,'SmsTemplate', 'Body', f.Body FROM [Nopforest].[SmsTemplate]  s
	INNER JOIN @TempFa f
	ON s.[Name] = f.[Name]

UPDATE st SET st.Body = f.Body FROM [Nopforest].[SmsTemplate] st INNER JOIN  

@TempFa f ON
st.Name = f.Name  
 