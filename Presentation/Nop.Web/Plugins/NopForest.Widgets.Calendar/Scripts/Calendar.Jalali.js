﻿String.prototype.toPersianDigits = function () {
    var id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    return this.replace(/[0-9]/g, function (w) {
        return id[+w];
    });
};
String.prototype.toEnglishDigits = function () {
    var id = { '۰': '0', '۱': '1', '۲': '2', '۳': '3', '۴': '4', '۵': '5', '۶': '6', '۷': '7', '۸': '8', '۹': '9' };
    return this.replace(/[^0-9.]/g, function (w) {
        return id[w] || w;
    });
};
Number.prototype.toTowDigit = function () {
    return this > 9 ? this : "0" + this;
}
$(document).ready(function () {

    // $.ajaxSetup({
        // dataFilter: function (data, type) {
            // //modify your data here
            // if (type == "json") {
                // var p1 = /[0-9]{4}-(0[1-9]|1[0-2]|[1-9])-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9])T([01][0-9]|2[0-4]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9]).[0-9]+/g;
                // data = data.replace(p1, function () { return "2019-01-01" });
            // }

            // return data;
        // }
    // });

    function fixNumbers() {
        var object = (($(".numberic-classes") || {}).data("numberic-classes") || "").split(',') || [];
		for(var i = 0; i < object.length; i++){
			$(object[i]).persiaNumber();
		}
		
        return this;
    }
    fixNumbers();
    $(document).ajaxComplete(function () {
        fixNumbers();
    });



    $(".main-footer .text-center").text(new persianDate().format("dddd, DD MMMM YYYY, h:mm:ss a"));
    var grids = $('[data-role="grid"]');
    var gridCount = grids.length;
    window.dataSource_requestEnd = function (e) {
        if (!this.grid)
            return;
        if (this.options.fields)
            for (var f = 0; f < this.options.fields.length; f++) {
                if (this.options.fields[f].type == "date") {
                    this.options.fields[f].format = "";
                }

            }
        this.grid.refresh();
        if (e.type === "read") {
            var data = e.response.Data;
            this.grid.columns.filter(function (v) { return v.type == "date" }).forEach(function (v) {
                for (var j = 0; j < data.length; j++) {
                    v.format = "";
                    if (data[j][v.field]) {
                        data[j][v.field + 'en'] = data[j][v.field];
                        data[j][v.field] = new persianDate(new Date(data[j][v.field])).toLocale('en').format();
                    }
                }
            });
        }
    };
    for (var l = 0; l < gridCount; l++) {
        var grid = $(grids[l]).data("kendoGrid");
        if (!grid) continue;
        grid.dataSource.grid = grid;
        grid.dataSource.bind("requestEnd", window.dataSource_requestEnd);
        //
        grid.columns.filter(function (v) { return v.type == "date" }).forEach(function (v) {
            v.editor = function (container,
                options) {
                var pdv = options.model[options.field + 'en'];
                var dp = container[0];
                var pde = $("<input type='text' style='max-width:100px; direction:ltr;text-align:left'  class='form-control' autocomplete='off' data-date data-persian-date/>");
                console.log(pde);
                pde.change(function (v) {
                    console.log(v);
                });
                pde.appendTo(dp);
                var pd = pde.persianDatepicker({
                    //initialValue: true,
                    onlySelectOnDate: false,
                    format: "YYYY/MM/DD",
                    initialValueType: 'gregorian',
                    onSelect: function (unix) {
                        options.model[options.field] = new persianDate(new Date(unix)).toLocale("en").format("YYYY-MM-DD");
                        options.model[options.field + 'en'] = new Date(unix);
                    },
                    onShow: function () {

                    }
                });
                pd.setDate(new Date(pdv));
            }
        });//end foreach
    };//end for

    $(document).ready(function () {
        var dps = $(".k-datepicker");
        if (dps.length) {
            dps.each(function (i) {
                // dps.css("display", "none");
                var dp = $(dps[i]);
                var pde = $("<input type='text' style='direction:ltr;text-align:left' class='form-control' autocomplete='off' data-date data-persian-date/>");
                dp.before(pde);
                var pr = $("<div class='input-group'></div>");
                pde.wrap(pr);
                console.log(pde);
                pde.change(function (v) {
                    console.log(v);
                });

                function toDate(date) {
                    return date.getFullYear() +
                        "/" +
                        (date.getMonth() + 1).toTowDigit() +
                        "/" +
                        date.getDate().toTowDigit();
                }
                var pd = pde.persianDatepicker({
                    initialValue: false,
                    onlySelectOnDate: false,
                    format: "YYYY/MM/DD",
                    initialValueType: 'gregorian',
                    onSelect: function (unix) {
                        var date;
                        if (unix)
                            date = new Date(unix);
                        else
                            date = new persianDate(pde.val().toEnglishDigits().split(/[\/\s\:]/)
                                .map(function (n) { return parseInt(n) })).toDate();
                        dp.find(".k-input").val(toDate(date));
                    },
                    onShow: function () {

                    }
                });

                var val = $(dps[i]).find('.k-input').val();
                function fixDate(val) {
                    var ov = val;
                    var darr = val.toEnglishDigits().split(/[\/\s\:]/)
                        .map(function (n) { return parseInt(n) });
                    if (darr[0] < 32) {
                        var t = darr[0];
                        darr[0] = darr[2];
                        darr[2] = t;
                    }
                    if (darr[0] < 1500) {
                        //   console.log(darr);
                        //if date value is jalali calendar
                        var date = new persianDate(darr);
                        pd.setDate(new persianDate(darr).toDate());
                        dp.find(".k-input").val(toDate(date.toDate()));
                        pde.val(date.format("YYYY/MM/DD"));
                    } else {
                        //if date value is gregorian calendar
                        if (isNaN(Date.parse(ov))) {
                            ov = darr[0] + "/" + darr[1].toTowDigit() + "/" + darr[2].toTowDigit();
                        }
                        //   console.log(ov);

                        pde.val(ov);

                        dp.find(".k-input").val(ov);
                        pd.setDate(Date.parse(ov));
                    }
                }

                pde.on("keydown",
                    function (e) {
                        console.log(e);
                    });
                pde.on("keyup", function (v) {
                    var val = pde.val();
                    if (/\d{4}\/\d{2}\/\d{2}$/gm.exec(val) != null) {

                        val = val + " ";
                    }
                    else if (/\d{4}$/gm.exec(val) != null || /\d{4}\/\d{2}$/gm.exec(val) != null) {

                        val = val + "/";
                    }
                    pde.val(val);

                    var regex = /\d{4}\/\d{2}\/\d{2}/gm;
                    if (regex.exec(pde.val()) != null) {
                        fixDate(pde.val());
                    }
                    else {
                        dp.find(".k-input").val(null);
                    }
                    console.log(val);
                });

                if (val) {
                    fixDate(val);
                } else {
                    //TODO
                  /// pd.setDate(null);
                }

            });
        }
        var dps = $(".k-datetimepicker");
        if (dps.length) {
            dps.each(function (i) {
                var dp = $(dps[i]);
                var pde = $("<input type='text' style='direction:ltr;text-align:left' pattern='[0-9]*' class='form-control' autocomplete='off' data-date data-persian-date/>");
                dp.before(pde);
                console.log(pde);

                var pr = $("<div class='input-group' ></div>");
                pde.wrap(pr);
                function toDate(date) {
                    return date.getFullYear() +
                        "/" +
                        (date.getMonth() + 1).toTowDigit() +
                        "/" +
                        date.getDate().toTowDigit() +
                        " " +
                        ((date.getHours().toTowDigit() % 12) === 0
                            ? 12 : ((date.getHours().toTowDigit() % 12)) +
                            ":" +
                            date.getMinutes().toTowDigit() +
                            " " + (
                                date.getHours().toTowDigit() <
                                    12
                                    ? "AM"
                                    : "PM"));

                }
                var pd = pde.persianDatepicker({
                    initialValue: false,
                    onlySelectOnDate: false,
                    format: "YYYY/MM/DD HH:mm:ss",
                    initialValueType: 'gregorian',
                    timePicker: {
                        enabled: true,
                        meridiem: {
                            enabled: true
                        }
                    },
                    onSelect: function (unix) {
                        var date;
                        if (unix)
                            date = new Date(unix);
                        else
                            date = new persianDate(pde.val().toEnglishDigits().split(/[\/\s\:]/)
                                .map(function (n) { return parseInt(n) })).toDate();
                        dp.find(".k-input").val(toDate(date));
                    },
                    onShow: function () {

                    }
                });
                var val = $(dps[i]).find('.k-input').val();
                function fixDate(val) {
                    var ov = val;
                    var base = 0;
                    if (val.indexOf("ب.ظ") > 0 || val.indexOf("P.M") > 0) {
                        base = 12;
                    }
                    val = val.replace("ب.ظ", "").replace("ق.ظ", "").replace("AM", "").replace("PM", "");
                    var darr = val.toEnglishDigits().split(/[\/\s\:]/)
                        .map(function (n) { return parseInt(n) });
                    darr[3] += base;
                    if (darr[0] < 32) {
                        var t = darr[0];
                        darr[0] = darr[2];
                        darr[2] = t;
                    }
                    if (darr[0] < 100) {
                        darr[0] = 1300 + darr[0];
                    }


                    if (darr[0] < 1500) {
                        //if date value is jalali calendar
                        console.log("//if date value is jalali calendar");
                        console.log(darr);
                        var date = new persianDate(darr);
                        pd.setDate(date.toDate());
                        console.log(dp.find(".k-input"));
                        console.log(date.toDate());
                        dp.find(".k-input").val(toDate(date.toDate()));
                        pde.val(date.format("YYYY/MM/DD HH:mm"));
                    } else {
                        //if date value is gregorian calendar
                        if (isNaN(Date.parse(ov))) {
                            ov = darr[0] + "/" + darr[1].toTowDigit() + "/" + darr[2].toTowDigit() + " " + darr[3].toTowDigit() + ":" + darr[4].toTowDigit() + ":" + darr[5].toTowDigit();
                        }
                        pde.val(ov);
                        dp.find(".k-input").val(ov);
                        pd.setDate(Date.parse(ov));
                    }
                }

                pde.on("keyup", function (v) {
                    var val = pde.val();
                    if (/\d{4}\/\d{2}\/\d{2}\s\d{2}$/gm.exec(val) != null) {
                        val = val + ":";
                    }
                    else
                        if (/\d{4}\/\d{2}\/\d{2}$/gm.exec(val) != null) {

                            val = val + " ";
                        }
                        else if (/\d{4}$/gm.exec(val) != null || /\d{4}\/\d{2}$/gm.exec(val) != null) {

                            val = val + "/";
                        }
                    pde.val(val);

                    var regex = /\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}/gm;
                    if (regex.exec(pde.val()) != null) {
                        fixDate(pde.val());
                    }
                    else {
                        dp.find(".k-input").val(null);
                    }
                    console.log(pde.val());
                });
                if (val) {
                    fixDate(val);
                } else {
                    //TODO
                    //pd.setDate(null);
                }
            });
        }
        //for brithday 
    });
    var searchMonthOfBirth = $("#SearchMonthOfBirth");
    var searchDayOfBirth = $("#SearchDayOfBirth");

    if (searchDayOfBirth.length && searchMonthOfBirth.length) {
        searchDayOfBirth.css("display", "none");
        searchMonthOfBirth.css("display", "none");
        var pSearchDayOfBirth = $("<select id='PSearchDayOfBirth' class='form-control valid'>").insertAfter(searchDayOfBirth);
        var pSearchMonthOfBirth = $("<select id='PSearchMonthOfBirth' class='form-control valid'>").insertAfter(searchMonthOfBirth);
        pSearchMonthOfBirth.append("<option value=0>ماه</option>");
        for (var i = 1; i < 13; i++) {
            pSearchMonthOfBirth.append("<option value=0" + i.toString() + ">" + i.toString() + "</option>");
        }
        pSearchDayOfBirth.append("<option value=0>روز</option>");
        for (var i = 1; i < 32; i++) {
            pSearchDayOfBirth.append("<option value= " + i.toString() + ">" + i.toString() + "</option>");
        }
        var udarr = function () {
            var day = $("#PSearchDayOfBirth  option:selected").val();
            var month = $("#PSearchMonthOfBirth  option:selected").val();
            if (day * month === 0) {
                if (day === 0) {
                    $("#SearchDayOfBirth").val(0).change();
                }
                if (month === 0) {
                    $("#SearchMonthOfBirth").val(0).change();;
                }
                return;
            }
            //   console.log("1362" + "/" + month + "/" + day);
            var date = (new persianDate([1362, month * 1, day * 1])).toDate();

            $("#SearchDayOfBirth").val(date.getDate()).change();
            $("#SearchMonthOfBirth").val(date.getMonth() + 1).change();;
        }
        pSearchMonthOfBirth.change(udarr);
        pSearchDayOfBirth.change(udarr);
    }
    var dps = $(".date-picker-wrapper");
    //console.log(dps.length);
    if (dps.length) {
        var pde = $("<input type='text' style='direction:ltr;text-align:left' class='form-control' autocomplete='off' data-persian-calendar/>");


        pde.insertAfter(dps.parent().children("label"));

        var day = $(".date-picker-wrapper [name=DateOfBirthDay]");
        var month = $(".date-picker-wrapper [name=DateOfBirthMonth]");
        var year = $(".date-picker-wrapper [name=DateOfBirthYear]");
        function getDate() {
            return (Math.floor(year.val()) || 1983).toString() + "/" + (Math.floor(month.val()) || 3).toString() + "/" + (Math.floor(day.val()) || 21).toString();
        };

        $("[data-persian-calendar]").attr("data-date", getDate());
        var pd = $("[data-persian-calendar]").persianDatepicker({
            initialValue: true,
            onlySelectOnDate: true,
            format: getDatetimeFormat(),
            initialValueType: 'gregorian',
            onSelect: function (unix) {
                var date = new Date(unix);
                day.val(date.getDate());
                month.val(date.getMonth() + 1);
                year.val(date.getFullYear());
            }
        });
        function fixDate(val) {
            var ov = val;
            var base = 0;
            if (val.indexOf("ب.ظ") > 0 || val.indexOf("P.M" > 0)) {
                base = 12;
            }

            val = val.replace("ب.ظ", "").replace("ق.ظ", "").replace("AM", "").replace("PM", "");
            var darr = val.toEnglishDigits().split(/[\/\s\:]/)
                .map(function (n) { return parseInt(n) });


            darr[3] += base;
            if (darr[0] < 32) {
                var t = darr[0];
                darr[0] = darr[2];
                darr[2] = t;
            }
            if (darr[0] < 100) {
                darr[0] = 1300 + darr[0];
            }


            if (darr[0] < 1500) {
                //if date value is jalali calendar
                console.log("//if date value is jalali calendar");
                console.log(darr);
                var date = new persianDate(darr);
                pd.setDate(date.toDate());
                console.log(dp.find(".k-input"));
                console.log(date.toDate());
                dp.find(".k-input").val(toDate(date.toDate()));
                pde.val(date.format("YYYY/MM/DD HH:mm"));
            } else {
                //if date value is gregorian calendar
                if (isNaN(Date.parse(ov))) {
                    ov = darr[0] + "/" + darr[1].toTowDigit() + "/" + darr[2].toTowDigit() + " " + darr[3].toTowDigit() + ":" + darr[4].toTowDigit() + ":" + darr[5].toTowDigit();
                }
                pde.val(ov);
                dp.find(".k-input").val(ov);
                pd.setDate(Date.parse(ov));
            }
        }

        pde.on("keyup", function (v) {
            var val = pde.val();
            if (/\d{4}\/\d{2}\/\d{2}\s\d{2}$/gm.exec(val) != null) {
                val = val + ":";
            }
            else
                if (/\d{4}\/\d{2}\/\d{2}$/gm.exec(val) != null) {

                    val = val + " ";
                }
                else if (/\d{4}$/gm.exec(val) != null || /\d{4}\/\d{2}$/gm.exec(val) != null) {

                    val = val + "/";
                }
            pde.val(val);

            var regex = /\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}/gm;
            if (regex.exec(pde.val()) != null) {
                fixDate(pde.val());
            }
            console.log(pde.val());
        });


        pd.setDate((new Date(getDate())).getTime());
    }
});
