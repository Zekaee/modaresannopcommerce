﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Core
{
    public class TrakingOrder
    {
        public string Mobile { get; set; }
        public string Date { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TrakingNo { get; set; }
        public string Destination { get; set; }
    }
}
