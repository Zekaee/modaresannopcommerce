﻿using SyncClient.Lib;
using SyncClient.Lib.NopModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncClient
{
    public partial class MainWindows : Form
    {
        private int LastId { get; set; }
        public SettingsModel SettingsModel = null;
        public MainWindows()
        {
            InitializeComponent();
        }

        private void MainWindows_Load(object sender, EventArgs e)
        {
            UpdateSettingView();
        }

        private void ToggleButton(object sender)
        {
            ((Button)sender).Enabled = !(((Button)sender).Enabled);
            ((Button)sender).Focus();
        }

        private async void AuthorizeBtn_Click(object sender, EventArgs e)
        {
            ToggleButton(sender);
            UpdateSettingView();
            var tokenModel = await new AuthorizeService().AuthAsync(SettingsModel.NopServerAddress, SettingsModel.NopUsername,
                SettingsModel.NopPassword);

            if (tokenModel == null)
            {
                MessageBox.Show(Errors.NullAuthError);
                ToggleButton(sender);
                return;
            }

            if (!tokenModel.Succeed)
                MessageBox.Show(tokenModel.LoginError);

            SettingsModel.NopAuthToken = tokenModel.Token;
            UpdateSettingView();
            ToggleButton(sender);
        }


        public void UpdateSettingsModel()
        {
            SettingsModel.AccUsername = AccUsername.Text;
            SettingsModel.AccPassword = AccPassword.Text;
            SettingsModel.AccServerAddress = AccServerAddress.Text;
            SettingsModel.NopServerAddress = NopServerAddressTxt.Text;
            SettingsModel.DefaultCustomerId = DefaultCustomerIdText.Text;
            SettingsModel.ShippingMethodId = ShippingMethodIdText.Text;
            // SettingsModel.StokeId = StokeIdText.Text;
            SettingsModel.SaleTypeId = SaleTypeIdText.Text;
            SettingsModel.AgencySaleTypeId = AgencySaleTypeIdText.Text;
        }

        public void UpdateSettingView()
        {
            SettingsModel = Lib.SettingsModel.LoadSettings();
            AccUsername.Text = SettingsModel.AccUsername;
            AccPassword.Text = SettingsModel.AccPassword;
            AccServerAddress.Text = SettingsModel.AccServerAddress;
            NopServerAddressTxt.Text = SettingsModel.NopServerAddress;
            DefaultCustomerIdText.Text = SettingsModel.DefaultCustomerId;
            ShippingMethodIdText.Text = SettingsModel.ShippingMethodId;
            //StokeIdText.Text = SettingsModel.StokeId;
            SaleTypeIdText.Text = SettingsModel.SaleTypeId;
            AgencySaleTypeIdText.Text = SettingsModel.AgencySaleTypeId;


        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            ToggleButton(sender);
            UpdateSettingsModel();
            SettingsModel.SaveSetting();
            ToggleButton(sender);
        }

        private async void SyncNowBtn_Click(object sender, EventArgs e)
        {
            ToggleButton(sender);
            try
            {
                await Task.Run(() =>
                {
                    SyncGoods();
                });
            }
            catch (Exception)
            {
            }
            ToggleButton(sender);
        }

        private void SyncGoods()
        {
            var goods = arianSystemService.GetGoods();
            var builder = new StringBuilder();
#if DEBUG
            var builder1 = new StringBuilder();
#endif
            Stack<string> queris = new Stack<string>();

            if (goods != null)
                using (SqlConnection sql = new System.Data.SqlClient.SqlConnection("Data Source=.;Initial Catalog=OnlineStore;Integrated Security=True;;Connection Timeout=600"))
                {
                    sql.Open();

                    var k = 0;
                    foreach (var item in goods.Res)
                    {
                        k++;

                        var good = GoodsResult.GetFromArray(item);
                        builder.AppendLine($"UPDATE [dbo].[Product] SET StockQuantity ={good.Quantity} WHERE Sku LIKE '32-{good.GoodsId}'");
                        if (k % 100 == 0)
                        {
                            try
                            {
                                using (var sqlCommand = new SqlCommand(builder.ToString(), sql))
                                {
                                    sqlCommand.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                return;
                            }
#if DEBUG
                            builder1.AppendLine(builder.ToString());
#endif
                            builder.Clear();
                        }

                    }
                    if (k % 100 != 0)
                        try
                        {
                            using (var sqlCommand = new SqlCommand(builder.ToString(), sql))
                            {
                                sqlCommand.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            return;
                        }
#if DEBUG
                    builder1.AppendLine(builder.ToString());
#endif
                    sql.Close();
                }

#if DEBUG
            System.IO.File.WriteAllText(Application.StartupPath + $"{DateTime.Now.ToString("YYYY-MM-dd HH-mm-dd.txt")}", builder1.ToString());
#endif
        }

        private async void timerSync_Tick(object sender, EventArgs e)
        {
            timerSync.Tick -= timerSync_Tick;
            if (ReportPanelRtb.Text.Length > 1024 * 20)
                ReportPanelRtb.Text = "";
            lblAlarm.Visible = true;
            try
            {
                await SyncOrders();

            }
            catch (Exception ex)
            {

                //throw;
            }
            lblAlarm.Visible = false;
            timerSync.Tick += timerSync_Tick;
        }

        public class CustomerInfo
        {
            public string GetStokeId() => (10000000 + int.Parse(this.StokeId)).ToString();
            public string StokeId { get; set; }
            public string AccCode { get; set; }
        }
        private class ProductSku
        {
            public ProductSku(bool error = false)
            {
                Error = error;
            }

            public string GetStokeId() => (10000000 + int.Parse(this.StokeId)).ToString();
            public static string GetStokeId(string stokeId) => (10000000 + int.Parse(stokeId)).ToString();
            public bool HasTrendSku() => !string.IsNullOrEmpty(TrendSku);
            public bool HasMainSku() => !string.IsNullOrEmpty(MainSku) && MainSku.StartsWith("1");
            public string StokeId { get; set; }
            public string MainSku { get; set; }
            public string TrendSku { get; set; }
            public string Sku => string.IsNullOrEmpty(MainSku) ? TrendSku : MainSku;
            public bool Error { get; set; }
            public int Id { get; set; }
        }
        CustomerInfo GetCustomerInfo(Order order1)
        {
            if (string.IsNullOrEmpty(order1.Customer.AdminComment) || !Regex.IsMatch(order1.Customer.AdminComment, @"(\d{2})-(\d{8})"))
            {
                //کاربر عادی
                return new CustomerInfo() { StokeId = "32", AccCode = SettingsModel.DefaultCustomerId };
            }
            var match = Regex.Match(order1.Customer.AdminComment, @"(\d{2})-(\d{8})");
            return new CustomerInfo() { AccCode = match.Groups[2].Value, StokeId = match.Groups[1].Value };

        }

        NopSyncService nopSyncService = new NopSyncService();
        ArianSystemService arianSystemService = new ArianSystemService();

        private async Task SyncOrders()
        {
            async Task<bool> SyncBatch()
            {
                var orders = await nopSyncService.GetOrders(limit: 10, sinceId: LastId, synced:false);
                if (orders == null)
                    return false;

                foreach (var order in orders.Orders)
                {
                    if (order == null)
                        continue;

                    LastId = order.Id;
                    if (order.OrderItems == null)
                        continue;

                    //بررسی می‌کنیم فاکتور کتاب است یا خیر

                    var skus = order.OrderItems.Select(x =>
                    {
                        return GetSku(x);
                    });

                    var hasShipping = true;
                    var customerInfo = GetCustomerInfo(order);
                    var result = new SyncResult() { Succeed = true };
                    var secondNumber = "";

                    hasShipping = order.OrderShippingExclTax > 0;

                    foreach (var g in skus.GroupBy(x => x.StokeId))
                    {
                        secondNumber = $"{order.Id + 1000000}";

                        var tempOrder = order.Clone();
                        //کالاهای هم انبار
                        tempOrder.OrderItems = (from o in order.OrderItems
                                                join gi in g on o.Id equals gi.Id
                                                where gi.StokeId == g.Key
                                                select o).ToList();

                        var stokeId = "";
                        //اگر کتاب بود کتاب و کتاب فروش بود
                        if (g.Key == "32" && customerInfo.StokeId == "30")
                            stokeId = "30";
                        else
                            stokeId = g.Key;
                        //var secondNumber = ProductSku.GetStokeId(stokeId);
                        double price = GetPrice(tempOrder, stokeId, out List<OrderDetail> orderDetails);
                        //Order order, double price, List<OrderDetail> orderDetails, bool shippingCoust, string customerId, string secondNumber, string stockId
                        result = await PushOrder(order: tempOrder, price: order.OrderTotal, orderDetails: orderDetails, shippingCoust: hasShipping, customerId: customerInfo.AccCode, secondNumber: secondNumber, stockId: stokeId);

                        if (result.ErrorMessage == "Error:    \r\nبعلت تنظيمات فرايند , ورود شماره فرعي تکراري در سند مجاز نيست. ")
                        {
                            result.Succeed = true;
                        }

                        if (!result.IsSucceed)
                        {
                            //اگر موفق نبود خارج می‌شویم
                            continue;
                        }
                        //برای اولین نوع محصول هزینه ارسال را محاسبه می‌کنیم
                        hasShipping = false;

                    }

                    if (!result.Succeed)
                    {
                        LogService.EventLog?.WriteEntry(result.ErrorMessage, EventLogEntryType.Error);
                    }

                    if (!result.IsSucceed)
                    {
                        await nopSyncService.AddOrderNote(order.Id, message: result.ErrorMessage);

                        ReportPanelRtb.Text += $@"Error {secondNumber}\n";
                    }
                    else
                    {
                        ReportPanelRtb.Text += $@"Succeed {order.Id}\n";
                        await nopSyncService.SycOrder(order.Id);
                    }



                    //if (order.Customer.FirstName.Length == 8 && Regex.IsMatch(order.Customer.FirstName, agencyPattern))
                    //{
                    //    order.Customer.AdminComment = order.Customer.FirstName;
                    //}


                    //var agencyAndBookStoreStokes = GetAgencyAndBookStoreStokes(order.Customer.AdminComment);


                }
                return orders.Orders.Count > 0;
            }
            LastId = 0;
            while (await SyncBatch())
            {
                //DO NOTHING :)
            }
        }

        private static ProductSku GetSku(OrderItem x)
        {
            //محصول مجازی است
            if (string.IsNullOrEmpty(x.Product.Sku))
            {
                var match = Regex.Match(input: x.AttributeDescription, @"((\d{2})-(1\d{7}))|((1\d{7})-(\d{2}))");
                if (!match.Success)
                    return new ProductSku(true);
                //کد-محصول را از ویژگی‌های محصول می‌خواند
                if (match.Groups[1].Success)
                    return new ProductSku() { Id = x.Id, StokeId = match.Groups[2].Value, TrendSku = match.Groups[3].Value };
                if (match.Groups[4].Success)
                    return new ProductSku() { Id = x.Id, StokeId = match.Groups[6].Value, TrendSku = match.Groups[5].Value };

            }
            {
                //کتاب است
                var match = Regex.Match(x.Product.Sku, @"(\d{2})-(1\d{7})");
                if (match.Success)
                {
                    return new ProductSku() { Id = x.Id, StokeId = match.Groups[1].Value, MainSku = match.Groups[2].Value };
                }//بسته با آزمون یا آزمون یا محصولات بسته بدون گرایشس
            }
            return new ProductSku(true);
        }

        private async Task<SyncResult> PushOrder(Order order, double price, List<OrderDetail> orderDetails, bool shippingCoust, string customerId, string secondNumber, string stockId)
        {

            var result = await arianSystemService.PushOrder(new PushOrderModel
            {
                CustomerId = customerId,
                SecondNumber = secondNumber,
                StockId = 100000 + stockId,
                ElementId = SettingsModel.ShippingMethodId,
                BankAccountId = order.CorrespondingAccountNumber,
                SaleTypeID = customerId == SettingsModel.DefaultCustomerId ? SettingsModel.SaleTypeId : SettingsModel.AgencySaleTypeId,
                VariziMab = price.ToString("##0"),
                ElementMab = shippingCoust ? order.OrderShippingExclTax.ToString("##0") : "0",
                VariziVoucherDate = ToPersian((order.PaidDateUtc ?? DateTime.Now)),
                FishNumber = string.IsNullOrEmpty(order.AuthorizationTransactionId) ? "000000" : order.AuthorizationTransactionId,
                Desc = $"بابت فاکتور شماره {order.Id} توسط {order.Customer.FirstName} {order.Customer.LastName} {order.Customer.Username} به شماره پیگیری {order.AuthorizationTransactionId}",
                OrderDetails = orderDetails
            });
            return result;
        }

        private static double GetPrice(Order order, string stokeId, out List<OrderDetail> orderDetails)
        {
            orderDetails = new List<OrderDetail>();

            const string pricePattern = @"\+([\d\,]+)\sریال";
            double price = 0;
            foreach (var orderOrderItem in order.OrderItems)
            {
                double price1 = 0;

                if (!string.IsNullOrEmpty(orderOrderItem.AttributeDescription))
                    if (Regex.IsMatch(orderOrderItem.AttributeDescription, pricePattern))
                        price1 = int.Parse(Regex.Match(orderOrderItem.AttributeDescription, pricePattern).Groups[1].Value.Replace(",", ""));

                price1 = orderOrderItem.Product.Price + price1;
                price += price1 * orderOrderItem.Quantity;
                orderDetails.Add(new OrderDetail()
                {
                    DiscountMab = "0",
                    Fee = price1.ToString("##0"),
                    GoodsId = GetSku(orderOrderItem).Sku,
                    Quantity = orderOrderItem.Quantity.ToString()
                });
            }
            return price;
        }

        string ToPersian(DateTime dateTime)
        {
            var pc = new PersianCalendar();
            return $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime):00}/{pc.GetDayOfMonth(dateTime):00}";
        }
        private void ShowReportCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ReportPanelRtb.Visible = !ReportPanelRtb.Visible;
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            timerSync.Enabled = true;
            goodsTimer.Enabled = true;
            timerSync.Interval = (int)syncInterval.Value * 1000;
            goodsTimer.Interval = (int)goodsInterval.Value * 1000;
            ToggleEnabled(StartBtn);
            ToggleEnabled(StopBtn);
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            timerSync.Enabled = false;
            ToggleEnabled(StartBtn);
            ToggleEnabled(StopBtn);
        }

        private void ToggleEnabled(Button btn)
        {
            btn.Enabled = !btn.Enabled;
        }

        private async void GoodsTimer_Tick(object sender, EventArgs e)
        {
            goodsTimer.Tick -= GoodsTimer_Tick;
            await Task.Run(() =>
            {
                SyncGoods();
            });
            goodsTimer.Tick += GoodsTimer_Tick;
        }
    }

    public class GoodsResult
    {
        public static GoodsItem GetFromArray(List<string> item)
        {
            //["10008361","00920909","روانشناسی رشد ارشد چ18","10000009"," کتاب کارشناسی ارشد","303"],
            return new GoodsItem()
            {
                GoodsId = item[0],
                GoodsCode = item[1],
                GoodsDesc = item[2],
                GroupId = item[3],
                GroupDesc = item[4],
                Quantity = item[5],
            };
        }
        public string Err { get; set; }
        public List<List<string>> Res { get; set; }
        public class GoodsItem
        {

            public string GoodsId { get; set; }
            public string GoodsCode { get; set; }
            public string GoodsDesc { get; set; }
            public string GroupId { get; set; }
            public string GroupDesc { get; set; }
            public string Quantity { get; set; }
        }

    }
}
