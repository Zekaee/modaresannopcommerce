﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncClient
{
    public static class LogService
    {
        public static EventLog EventLog { get; }

        static LogService()
        {
            // Create the source, if it does not already exist.
            //if (!EventLog.SourceExists("ArianSyncService"))
            //{
            //    //An event log source should not be created and immediately used.
            //    //There is a latency time to enable the source, it should be created
            //    //prior to executing the application that uses the source.
            //    //Execute this sample a second time to use the new source.
            //    EventLog.CreateEventSource("ArianSyncService", "ArianSyncServiceLog");
            //    // The source is created.  Exit the application to allow it to be registered.
            //    return;
            //}
            //// Create an EventLog instance and assign its source.
            //EventLog = new EventLog();
            //EventLog.Source = "ArianSyncService";
            // Write an informational entry to the event log.    
            //            EventLog.WriteEntry("Writing to event log.");
        }
    }
}
