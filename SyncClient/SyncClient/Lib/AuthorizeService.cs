﻿using Newtonsoft.Json;
using RestSharp;
using SyncClient.Lib.Models;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace SyncClient.Lib
{
    public class AuthorizeService
    {
        public async Task<UserModel> AuthAsync(string serverAddress, string username, string password)
        {
            var client = new RestClient($"{serverAddress}/api/auth?username={username}&password={password}");
            var request = new RestRequest(Method.POST);
            return await Task.Run<UserModel>(() =>
            {
                try
                {
                    var response = client.Execute(request);
                    return response.StatusCode == HttpStatusCode.OK ? JsonConvert.DeserializeObject<UserModel>(response.Content) : null;
                }
                catch (Exception ex)
                {
                    LogService.EventLog?.WriteEntry(ex.Message, EventLogEntryType.Error);
                    return null;
                }
            });
        }
    }
}
