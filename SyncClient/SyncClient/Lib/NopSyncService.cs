﻿using Newtonsoft.Json;
using RestSharp;
using SyncClient.Lib.NopModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SyncClient.Lib
{

    public class SyncResult
    {
        public bool Succeed { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsSucceed => Succeed || ErrorMessage == "Error :    \r\nبعلت تنظيمات فرايند , ورود شماره فرعي تکراري در سند مجاز نيست. ";
    }

    public class PushOrderModel
    {
        public string CustomerId { get; set; }
        public string SecondNumber { get; set; }
        public string StockId { get; set; }
        public string SaleTypeID { get; set; }
        public string BankAccountId { get; set; }
        public string FishNumber { get; set; }
        public string VariziMab { get; set; }
        public string VariziVoucherDate { get; set; }
        public string ElementId { get; set; }
        public string ElementMab { get; set; }
        public string Desc { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }


        public Dictionary<string, string> GetValue()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            keyValuePairs["Vals[0]"] = CustomerId;
            keyValuePairs["Vals[1]"] = SecondNumber;
            keyValuePairs["Vals[2]"] = StockId;
            keyValuePairs["Vals[3]"] = SaleTypeID;
            keyValuePairs["Vals[4]"] = BankAccountId;
            keyValuePairs["Vals[5]"] = FishNumber;
            keyValuePairs["Vals[6]"] = VariziMab;
            keyValuePairs["Vals[7]"] = VariziVoucherDate;
            keyValuePairs["Vals[8]"] = Desc;
            keyValuePairs["Vals[9]"] = ElementId;
            keyValuePairs["Vals[10]"] = ElementMab;
            return keyValuePairs;
        }

    }

    public class OrderDetail
    {
        public string GoodsId { get; set; }
        public string Quantity { get; set; }
        public string Fee { get; set; }
        public string DiscountMab { get; set; }
        public Dictionary<string, string> GetItems(int root)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            keyValuePairs[$"dets[{root}][0]"] = GoodsId;
            keyValuePairs[$"dets[{root}][1]"] = Quantity;
            keyValuePairs[$"dets[{root}][2]"] = Fee;
            keyValuePairs[$"dets[{root}][3]"] = DiscountMab;
            return keyValuePairs;
        }
    }


    public class ArianSystemService
    {
        private readonly SettingsModel _settingsModel = SettingsModel.LoadSettings();



        public async Task<SyncResult> PushOrder(PushOrderModel model)
        {
            return await Task.Run(() =>
            {
                if (!model.OrderDetails.Any())
                {
                    return new SyncResult()
                    {
                        Succeed = false,
                        ErrorMessage = "Order Details is empty"
                    };
                }
                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    try
                    {
                        var nv = new NameValueCollection();
                        nv.Add("UserName", _settingsModel.AccUsername);
                        nv.Add("PassWord", _settingsModel.AccPassword);
                        foreach (var item in model.GetValue())
                            nv.Add(item.Key, item.Value);

                        for (int i = 0; i < model.OrderDetails.Count; i++)
                            foreach (var it in model.OrderDetails[i].GetItems(i))
                                nv.Add(it.Key, it.Value);

                        Save(nv);

                        client.Headers.Add("X-Requested-With", "XMLHttpRequest");
                        var HtmlResult = System.Text.Encoding.UTF8.GetString(client.UploadValues($"{_settingsModel.AccServerAddress}/Web/SaleFactor", "Post", nv));

                        SaveError(HtmlResult);

                        var c = JsonConvert.DeserializeObject<Response>(HtmlResult);
                        return new SyncResult()
                        {
                            Succeed = string.IsNullOrEmpty(c.Err),
                            ErrorMessage = c.Err
                        };
                    }
                    catch (Exception exp)
                    {
                        return new SyncResult()
                        {
                            Succeed = false,
                            ErrorMessage = exp.Message
                        };
                    }
                }
            });
        }

        private void Save(NameValueCollection nv)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < nv.Count; i++)
                builder.AppendLine($"{ nv.AllKeys[i]}:{nv[nv.AllKeys[i]]}");
            var path = $"{AppContext.BaseDirectory}{Path.GetFileName(Path.GetTempFileName())}.txt";
            File.WriteAllText(path, builder.ToString());
        }

        private void SaveError(string error)
        {
            var path = $"{AppContext.BaseDirectory}{Path.GetFileName(Path.GetTempFileName())}.txt";
            File.WriteAllText(path, error);
        }

        internal GoodsResult GetGoods()
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Headers.Add("X-Requested-With", "XMLHttpRequest");
                    client.Encoding = System.Text.Encoding.UTF8;
                    string response = client.DownloadString($"{_settingsModel.AccServerAddress}/Web/SaleGoods?UserName={_settingsModel.AccUsername}&PassWord={_settingsModel.AccPassword}&PageNum=1&RowNum=10000&GoodsCode&BarCode");
                    return JsonConvert.DeserializeObject<GoodsResult>(response);
                }
                catch (Exception exp)
                {
                    return null;
                }
            }
        }
    }

    public class NopSyncService
    {
        const string Api = "d7d4da85-370e-47a1-8202-5b603b34bcb4";
        private readonly SettingsModel _settingsModel = SettingsModel.LoadSettings();
        public async Task<OrderRootObject> GetOrders(int limit = 50, int page = 0, int sinceId = 0, bool synced = false)
        {
            var client = new RestClient($"{_settingsModel.NopServerAddress}/Api/" + Api + $"/Orders?Limit={limit}&Page={page}&SinceId={sinceId}&Synced={synced}");
            var request = new RestRequest(Method.GET);
            //request.AddHeader("Authorization", $"Bearer {_settingsModel.NopAuthToken}");
            return await Task.Run(() =>
            {
                var response = client.Execute(request);
                try
                {
                    var rootObject = JsonConvert.DeserializeObject<OrderRootObject>(response.Content, new JsonSerializerSettings() {
                        NullValueHandling = NullValueHandling.Ignore,
                    });
                    if (response.StatusCode == HttpStatusCode.OK)
                        return rootObject;
                }
                catch (Exception e)
                {
                    LogService.EventLog?.WriteEntry(e.Message, EventLogEntryType.Error);
                }

                LogService.EventLog?.WriteEntry(response.ErrorMessage, EventLogEntryType.Error);
                return null;
            });
        }
        public async Task<bool> SycOrder(int id, string message = "Synced", bool unsync = false)
        {
            var client = new RestClient($"{_settingsModel.NopServerAddress}/api/" + Api + $"/orders/sync/{id}?message={message}&unsync={unsync}");
            var request = new RestRequest(Method.POST);
            //request.AddHeader("Authorization", $"Bearer {_settingsModel.NopAuthToken}");
            return await Task.Run(() =>
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                    return string.Equals(response.Content, "true", StringComparison.InvariantCultureIgnoreCase);
                LogService.EventLog?.WriteEntry(response.ErrorMessage, EventLogEntryType.Error);

                return false;
            });
        }
        public async Task<bool> AddOrderNote(int id, string message = "Synced")
        {
            var client = new RestClient($"{_settingsModel.NopServerAddress}/api/" + Api + $"/orders/note/{id}?message={message}");
            var request = new RestRequest(Method.POST);
            // request.AddHeader("Authorization", $"Bearer {_settingsModel.NopAuthToken}");
            return await Task.Run(() =>
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                    return string.Equals(response.Content, "true", StringComparison.InvariantCultureIgnoreCase);
                LogService.EventLog?.WriteEntry(response.ErrorMessage, EventLogEntryType.Error);
                return false;
            });
        }
    }
}
