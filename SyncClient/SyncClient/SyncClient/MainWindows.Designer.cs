﻿namespace SyncClient
{
    partial class MainWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.AuthorizeBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.SyncNowBtn = new System.Windows.Forms.Button();
            this.StartBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.ReportPanelRtb = new System.Windows.Forms.RichTextBox();
            this.ShowReportCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DefaultCustomerIdText = new System.Windows.Forms.TextBox();
            this.ShippingMethodIdText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SaleTypeIdText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AccServerAddress = new SyncClient.EndPointEdit();
            this.AccPassword = new System.Windows.Forms.TextBox();
            this.AccUsername = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.NopServerAddressTxt = new SyncClient.EndPointEdit();
            this.timerSync = new System.Windows.Forms.Timer(this.components);
            this.syncInterval = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAlarm = new System.Windows.Forms.Label();
            this.goodsTimer = new System.Windows.Forms.Timer(this.components);
            this.goodsInterval = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.syncInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Server Address";
            // 
            // AuthorizeBtn
            // 
            this.AuthorizeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AuthorizeBtn.Location = new System.Drawing.Point(12, 379);
            this.AuthorizeBtn.Name = "AuthorizeBtn";
            this.AuthorizeBtn.Size = new System.Drawing.Size(75, 23);
            this.AuthorizeBtn.TabIndex = 10;
            this.AuthorizeBtn.Text = "Authorize";
            this.AuthorizeBtn.UseVisualStyleBackColor = true;
            this.AuthorizeBtn.Click += new System.EventHandler(this.AuthorizeBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveBtn.Location = new System.Drawing.Point(93, 379);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveBtn.TabIndex = 11;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // SyncNowBtn
            // 
            this.SyncNowBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SyncNowBtn.Location = new System.Drawing.Point(174, 379);
            this.SyncNowBtn.Name = "SyncNowBtn";
            this.SyncNowBtn.Size = new System.Drawing.Size(75, 23);
            this.SyncNowBtn.TabIndex = 12;
            this.SyncNowBtn.Text = "Sync now";
            this.SyncNowBtn.UseVisualStyleBackColor = true;
            this.SyncNowBtn.Click += new System.EventHandler(this.SyncNowBtn_Click);
            // 
            // StartBtn
            // 
            this.StartBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StartBtn.Location = new System.Drawing.Point(255, 379);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(75, 23);
            this.StartBtn.TabIndex = 13;
            this.StartBtn.Text = "Start";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StopBtn.Enabled = false;
            this.StopBtn.Location = new System.Drawing.Point(336, 379);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(75, 23);
            this.StopBtn.TabIndex = 14;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // ReportPanelRtb
            // 
            this.ReportPanelRtb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReportPanelRtb.DataBindings.Add(new System.Windows.Forms.Binding("Visible", global::SyncClient.Properties.Settings.Default, "ShowReportBool", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ReportPanelRtb.Location = new System.Drawing.Point(536, 9);
            this.ReportPanelRtb.Name = "ReportPanelRtb";
            this.ReportPanelRtb.Size = new System.Drawing.Size(293, 393);
            this.ReportPanelRtb.TabIndex = 15;
            this.ReportPanelRtb.Text = "";
            this.ReportPanelRtb.Visible = global::SyncClient.Properties.Settings.Default.ShowReportBool;
            // 
            // ShowReportCheckBox
            // 
            this.ShowReportCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ShowReportCheckBox.AutoSize = true;
            this.ShowReportCheckBox.Checked = global::SyncClient.Properties.Settings.Default.ShowReportBool;
            this.ShowReportCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SyncClient.Properties.Settings.Default, "ShowReportBool", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ShowReportCheckBox.Location = new System.Drawing.Point(12, 356);
            this.ShowReportCheckBox.Name = "ShowReportCheckBox";
            this.ShowReportCheckBox.Size = new System.Drawing.Size(88, 17);
            this.ShowReportCheckBox.TabIndex = 9;
            this.ShowReportCheckBox.Text = "Show Report";
            this.ShowReportCheckBox.UseVisualStyleBackColor = true;
            this.ShowReportCheckBox.CheckedChanged += new System.EventHandler(this.ShowReportCheckBox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.DefaultCustomerIdText);
            this.groupBox1.Controls.Add(this.ShippingMethodIdText);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.SaleTypeIdText);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.AccServerAddress);
            this.groupBox1.Controls.Add(this.AccPassword);
            this.groupBox1.Controls.Add(this.AccUsername);
            this.groupBox1.Location = new System.Drawing.Point(12, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 236);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Accounting Server";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Shipping Method Id";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Default Customer Id";
            // 
            // DefaultCustomerIdText
            // 
            this.DefaultCustomerIdText.Location = new System.Drawing.Point(111, 149);
            this.DefaultCustomerIdText.Name = "DefaultCustomerIdText";
            this.DefaultCustomerIdText.Size = new System.Drawing.Size(286, 20);
            this.DefaultCustomerIdText.TabIndex = 12;
            // 
            // ShippingMethodIdText
            // 
            this.ShippingMethodIdText.Location = new System.Drawing.Point(111, 123);
            this.ShippingMethodIdText.Name = "ShippingMethodIdText";
            this.ShippingMethodIdText.Size = new System.Drawing.Size(286, 20);
            this.ShippingMethodIdText.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Sale Type Id";
            // 
            // SaleTypeIdText
            // 
            this.SaleTypeIdText.Location = new System.Drawing.Point(111, 97);
            this.SaleTypeIdText.Name = "SaleTypeIdText";
            this.SaleTypeIdText.Size = new System.Drawing.Size(286, 20);
            this.SaleTypeIdText.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Server Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Username";
            // 
            // AccServerAddress
            // 
            this.AccServerAddress.Location = new System.Drawing.Point(111, 71);
            this.AccServerAddress.Name = "AccServerAddress";
            this.AccServerAddress.Size = new System.Drawing.Size(286, 20);
            this.AccServerAddress.TabIndex = 8;
            this.AccServerAddress.Text = "http://127.0.0.1:8181";
            // 
            // AccPassword
            // 
            this.AccPassword.Location = new System.Drawing.Point(111, 45);
            this.AccPassword.Name = "AccPassword";
            this.AccPassword.PasswordChar = '*';
            this.AccPassword.Size = new System.Drawing.Size(286, 20);
            this.AccPassword.TabIndex = 7;
            // 
            // AccUsername
            // 
            this.AccUsername.Location = new System.Drawing.Point(111, 19);
            this.AccUsername.Name = "AccUsername";
            this.AccUsername.Size = new System.Drawing.Size(286, 20);
            this.AccUsername.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NopServerAddressTxt);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(403, 70);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nop Server";
            // 
            // NopServerAddressTxt
            // 
            this.NopServerAddressTxt.Location = new System.Drawing.Point(111, 19);
            this.NopServerAddressTxt.Name = "NopServerAddressTxt";
            this.NopServerAddressTxt.Size = new System.Drawing.Size(286, 20);
            this.NopServerAddressTxt.TabIndex = 3;
            this.NopServerAddressTxt.Text = "http://127.0.0.1:8181";
            // 
            // timerSync
            // 
            this.timerSync.Interval = 30000;
            this.timerSync.Tick += new System.EventHandler(this.timerSync_Tick);
            // 
            // syncInterval
            // 
            this.syncInterval.Location = new System.Drawing.Point(217, 354);
            this.syncInterval.Name = "syncInterval";
            this.syncInterval.Size = new System.Drawing.Size(95, 20);
            this.syncInterval.TabIndex = 16;
            this.syncInterval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Sync Time";
            // 
            // lblAlarm
            // 
            this.lblAlarm.AutoSize = true;
            this.lblAlarm.BackColor = System.Drawing.Color.Red;
            this.lblAlarm.Location = new System.Drawing.Point(417, 384);
            this.lblAlarm.Name = "lblAlarm";
            this.lblAlarm.Size = new System.Drawing.Size(43, 13);
            this.lblAlarm.TabIndex = 18;
            this.lblAlarm.Text = "            ";
            this.lblAlarm.Visible = false;
            // 
            // goodsTimer
            // 
            this.goodsTimer.Interval = 30000;
            this.goodsTimer.Tick += new System.EventHandler(this.GoodsTimer_Tick);
            // 
            // goodsInterval
            // 
            this.goodsInterval.Location = new System.Drawing.Point(320, 354);
            this.goodsInterval.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.goodsInterval.Name = "goodsInterval";
            this.goodsInterval.Size = new System.Drawing.Size(95, 20);
            this.goodsInterval.TabIndex = 19;
            this.goodsInterval.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // MainWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 414);
            this.Controls.Add(this.goodsInterval);
            this.Controls.Add(this.lblAlarm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.syncInterval);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ShowReportCheckBox);
            this.Controls.Add(this.ReportPanelRtb);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.StartBtn);
            this.Controls.Add(this.SyncNowBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.AuthorizeBtn);
            this.Name = "MainWindows";
            this.Text = " Sysn NopCommerce";
            this.Load += new System.EventHandler(this.MainWindows_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.syncInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goodsInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private EndPointEdit NopServerAddressTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button AuthorizeBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button SyncNowBtn;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.RichTextBox ReportPanelRtb;
        private System.Windows.Forms.CheckBox ShowReportCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private EndPointEdit AccServerAddress;
        private System.Windows.Forms.TextBox AccPassword;
        private System.Windows.Forms.TextBox AccUsername;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Timer timerSync;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ShippingMethodIdText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SaleTypeIdText;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox DefaultCustomerIdText;
        private System.Windows.Forms.NumericUpDown syncInterval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAlarm;
        private System.Windows.Forms.Timer goodsTimer;
        private System.Windows.Forms.NumericUpDown goodsInterval;
    }
}

