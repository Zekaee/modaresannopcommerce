﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SyncClient.Lib.NopModels
{
    public class Customer
    {
        [JsonProperty("customer_guid")] public string CustomerGuid { get; set; }
        [JsonProperty("username")] public string Username { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("language_id")] public object LanguageId { get; set; }
        [JsonProperty("date_of_birth")] public DateTime? DateOfBirth { get; set; }
        [JsonProperty("gender")] public string Gender { get; set; }
        [JsonProperty("admin_comment")] public string AdminComment { get; set; }
        [JsonProperty("is_tax_exempt")] public string IsTaxExempt { get; set; }
        [JsonProperty("has_shopping_cart_items")] public object HasShoppingCartItems { get; set; }
        [JsonProperty("active")] public object Active { get; set; }
        [JsonProperty("deleted")] public object Deleted { get; set; }
        [JsonProperty("is_system_account")] public object IsSystemAccount { get; set; }
        [JsonProperty("system_name")] public object SystemName { get; set; }
        [JsonProperty("last_ip_address")] public object LastIpAddress { get; set; }
        [JsonProperty("created_on_utc")] public object CreatedOnUtc { get; set; }
        [JsonProperty("last_login_date_utc")] public object LastLoginDateUtc { get; set; }
        [JsonProperty("last_activity_date_utc")] public object LastActivityDateUtc { get; set; }
        [JsonProperty("registered_in_store_id")] public object RegisteredInStoreId { get; set; }
        [JsonProperty("subscribed_to_newsletter")] public bool SubscribedToNewsletter { get; set; }
        public List<object> RoleIds { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
    }

    public class StateProvince
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
    }

    public class BillingAddress
    {
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("company")] public object Company { get; set; }
        [JsonProperty("CountryId")] public int CountryId { get; set; }
        [JsonProperty("StateProvinceId")] public int? StateProvinceId { get; set; }
        [JsonProperty("county")] public object County { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("address1")] public string Address1 { get; set; }
        [JsonProperty("address2")] public string Address2 { get; set; }
        [JsonProperty("zip_postal_code")] public string ZipPostalCode { get; set; }
        [JsonProperty("phone_number")] public string PhoneNumber { get; set; }
        [JsonProperty("fax_number")] public string FaxNumber { get; set; }
        [JsonProperty("custom_attributes")] public string CustomAttributes { get; set; }
        [JsonProperty("state_province")] public StateProvince StateProvince { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("state_province_name")] public string StateProvinceName { get; set; }
    }

    public class StateProvince2
    {
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
    }

    public class ShippingAddress
    {
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("company")] public object Company { get; set; }
        [JsonProperty("CountryId")] public int CountryId { get; set; }
        [JsonProperty("StateProvinceId")] public int? StateProvinceId { get; set; }
        [JsonProperty("county")] public object County { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("address1")] public string Address1 { get; set; }
        [JsonProperty("address2")] public string Address2 { get; set; }
        [JsonProperty("zip_postal_code")] public string ZipPostalCode { get; set; }
        [JsonProperty("phone_number")] public string PhoneNumber { get; set; }
        [JsonProperty("fax_number")] public string FaxNumber { get; set; }
        [JsonProperty("custom_attributes")] public string CustomAttributes { get; set; }
        [JsonProperty("state_province")] public StateProvince2 StateProvince { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("state_province_name")] public string StateProvinceName { get; set; }
    }

    public class Product
    {
        [JsonProperty("visible_individually")] public bool VisibleIndividually { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("short_description")] public string ShortDescription { get; set; }
        [JsonProperty("full_description")] public string FullDescription { get; set; }
        [JsonProperty("show_on_home_page")] public bool ShowOnHomePage { get; set; }
        [JsonProperty("meta_keywords")] public string MetaKeywords { get; set; }
        [JsonProperty("meta_description")] public string MetaDescription { get; set; }
        [JsonProperty("meta_title")] public string MetaTitle { get; set; }
        [JsonProperty("allow_customer_reviews")] public bool AllowCustomerReviews { get; set; }
        [JsonProperty("approved_rating_sum")] public int ApprovedRatingSum { get; set; }
        [JsonProperty("not_approved_rating_sum")] public int NotApprovedRatingSum { get; set; }
        [JsonProperty("approved_total_reviews")] public int ApprovedTotalReviews { get; set; }
        [JsonProperty("not_approved_total_reviews")] public int NotApprovedTotalReviews { get; set; }
        [JsonProperty("sku")] public string Sku { get; set; }
        [JsonProperty("manufacturer_part_number")] public object ManufacturerPartNumber { get; set; }
        [JsonProperty("gtin")] public object Gtin { get; set; }
        [JsonProperty("is_gift_card")] public bool IsGiftCard { get; set; }
        [JsonProperty("require_other_products")] public bool RequireOtherProducts { get; set; }
        [JsonProperty("automatically_add_required_products")] public bool AutomaticallyAddRequiredProducts { get; set; }
        [JsonProperty("is_download")] public bool IsDownload { get; set; }
        [JsonProperty("unlimited_downloads")] public bool UnlimitedDownloads { get; set; }
        [JsonProperty("max_number_of_downloads")] public int MaxNumberOfDownloads { get; set; }
        [JsonProperty("download_expiration_days")] public object DownloadExpirationDays { get; set; }
        [JsonProperty("has_sample_download")] public bool HasSampleDownload { get; set; }
        [JsonProperty("has_user_agreement")] public bool HasUserAgreement { get; set; }
        [JsonProperty("is_recurring")] public bool IsRecurring { get; set; }
        [JsonProperty("recurring_cycle_length")] public int RecurringCycleLength { get; set; }
        [JsonProperty("recurring_total_cycles")] public int RecurringTotalCycles { get; set; }
        [JsonProperty("is_rental")] public bool IsRental { get; set; }
        [JsonProperty("rental_price_length")] public int RentalPriceLength { get; set; }
        [JsonProperty("is_ship_enabled")] public bool IsShipEnabled { get; set; }
        [JsonProperty("is_free_shipping")] public bool IsFreeShipping { get; set; }
        [JsonProperty("ship_separately")] public bool ShipSeparately { get; set; }
        [JsonProperty("additional_shipping_charge")] public double AdditionalShippingCharge { get; set; }
        [JsonProperty("is_tax_exempt")] public bool IsTaxExempt { get; set; }
        [JsonProperty("is_telecommunications_or_broadcasting_or_electronic_services")] public bool IsTelecommunicationsOrBroadcastingOrElectronicServices { get; set; }
        [JsonProperty("use_multiple_warehouses")] public bool UseMultipleWarehouses { get; set; }
        [JsonProperty("manage_inventory_method_id")] public int ManageInventoryMethodId { get; set; }
        [JsonProperty("stock_quantity")] public int StockQuantity { get; set; }
        [JsonProperty("display_stock_availability")] public bool DisplayStockAvailability { get; set; }
        [JsonProperty("display_stock_quantity")] public bool DisplayStockQuantity { get; set; }
        [JsonProperty("min_stock_quantity")] public int MinStockQuantity { get; set; }
        [JsonProperty("notify_admin_for_quantity_below")] public int NotifyAdminForQuantityBelow { get; set; }
        [JsonProperty("allow_back_in_stock_subscriptions")] public bool AllowBackInStockSubscriptions { get; set; }
        [JsonProperty("order_minimum_quantity")] public int OrderMinimumQuantity { get; set; }
        [JsonProperty("order_maximum_quantity")] public int OrderMaximumQuantity { get; set; }
        [JsonProperty("allowed_quantities")] public object AllowedQuantities { get; set; }
        [JsonProperty("allow_adding_only_existing_attribute_combinations")] public bool AllowAddingOnlyExistingAttributeCombinations { get; set; }
        [JsonProperty("disable_buy_button")] public bool DisableBuyButton { get; set; }
        [JsonProperty("disable_wishlist_button")] public bool DisableWishlistButton { get; set; }
        [JsonProperty("available_for_pre_order")] public bool AvailableForPreOrder { get; set; }
        [JsonProperty("pre_order_availability_start_date_time_utc")] public object PreOrderAvailabilityStartDateTimeUtc { get; set; }
        [JsonProperty("call_for_price")] public bool CallForPrice { get; set; }
        [JsonProperty("price")] public double Price { get; set; }
        [JsonProperty("old_price")] public double OldPrice { get; set; }
        [JsonProperty("product_cost")] public double ProductCost { get; set; }
        [JsonProperty("special_price")] public object SpecialPrice { get; set; }
        [JsonProperty("special_price_start_date_time_utc")] public object SpecialPriceStartDateTimeUtc { get; set; }
        [JsonProperty("special_price_end_date_time_utc")] public object SpecialPriceEndDateTimeUtc { get; set; }
        [JsonProperty("customer_enters_price")] public bool CustomerEntersPrice { get; set; }
        [JsonProperty("minimum_customer_entered_price")] public double MinimumCustomerEnteredPrice { get; set; }
        [JsonProperty("maximum_customer_entered_price")] public double MaximumCustomerEnteredPrice { get; set; }
        [JsonProperty("baseprice_enabled")] public bool BasepriceEnabled { get; set; }
        [JsonProperty("baseprice_amount")] public double BasepriceAmount { get; set; }
        [JsonProperty("baseprice_base_amount")] public double BasepriceBaseAmount { get; set; }
        [JsonProperty("has_tier_prices")] public bool HasTierPrices { get; set; }
        [JsonProperty("has_discounts_applied")] public bool HasDiscountsApplied { get; set; }
        [JsonProperty("weight")] public double Weight { get; set; }
        [JsonProperty("length")] public double Length { get; set; }
        [JsonProperty("width")] public double Width { get; set; }
        [JsonProperty("height")] public double Height { get; set; }
        [JsonProperty("available_start_date_time_utc")] public object AvailableStartDateTimeUtc { get; set; }
        [JsonProperty("available_end_date_time_utc")] public object AvailableEndDateTimeUtc { get; set; }
        [JsonProperty("display_order")] public int DisplayOrder { get; set; }
        [JsonProperty("published")] public bool Published { get; set; }
        [JsonProperty("deleted")] public bool Deleted { get; set; }
        [JsonProperty("created_on_utc")] public DateTime CreatedOnUtc { get; set; }
        [JsonProperty("updated_on_utc")] public DateTime UpdatedOnUtc { get; set; }
        [JsonProperty("product_type")] public string ProductType { get; set; }
        [JsonProperty("parent_grouped_product_id")] public int ParentGroupedProductId { get; set; }
        public List<object> RoleIds { get; set; }
        public List<object> DiscountIds { get; set; }
        public List<object> StoreIds { get; set; }
        public List<object> ManufacturerIds { get; set; }
        public List<object> Attributes { get; set; }
        public List<object> ProductAttributeCombinations { get; set; }
        public List<object> AssociatedProductIds { get; set; }
        public List<object> Tags { get; set; }
        [JsonProperty("VendorId")] public int VendorId { get; set; }
        [JsonProperty("se_name")] public string SeName { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
    }

    public class OrderItem
    {
        public List<object> ProductAttributes { get; set; }
        [JsonProperty("quantity")] public int Quantity { get; set; }
        [JsonProperty("unit_price_incl_tax")] public double UnitPriceInclTax { get; set; }
        [JsonProperty("unit_price_excl_tax")] public double UnitPriceExclTax { get; set; }
        [JsonProperty("price_incl_tax")] public double PriceInclTax { get; set; }
        [JsonProperty("price_excl_tax")] public double PriceExclTax { get; set; }
        [JsonProperty("discount_amount_incl_tax")] public double DiscountAmountInclTax { get; set; }
        [JsonProperty("discount_amount_excl_tax")] public double DiscountAmountExclTax { get; set; }
        [JsonProperty("original_product_cost")] public double OriginalProductCost { get; set; }
        [JsonProperty("attribute_description")] public string AttributeDescription { get; set; }
        [JsonProperty("download_count")] public int DownloadCount { get; set; }
        [JsonProperty("isDownload_activated")] public bool IsDownloadActivated { get; set; }
        [JsonProperty("license_download_id")] public int LicenseDownloadId { get; set; }
        [JsonProperty("item_weight")] public double ItemWeight { get; set; }
        [JsonProperty("rental_start_date_utc")] public object RentalStartDateUtc { get; set; }
        [JsonProperty("rental_end_date_utc")] public object RentalEndDateUtc { get; set; }
        [JsonProperty("product")] public Product Product { get; set; }
        [JsonProperty("product_id")] public int ProductId { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
    }

    public class Order
    {
        [JsonProperty("store_id")] public int StoreId { get; set; }
        [JsonProperty("pick_up_in_store")] public bool PickUpInStore { get; set; }
        [JsonProperty("payment_method_system_name")] public string PaymentMethodSystemName { get; set; }
        [JsonProperty("customer_currency_code")] public string CustomerCurrencyCode { get; set; }
        [JsonProperty("currency_rate")] public double CurrencyRate { get; set; }
        [JsonProperty("customer_tax_display_type_id")] public int CustomerTaxDisplayTypeId { get; set; }
        [JsonProperty("vat_number")] public object VatNumber { get; set; }
        [JsonProperty("order_subtotal_incl_tax")] public double OrderSubtotalInclTax { get; set; }
        [JsonProperty("order_subtotal_excl_tax")] public double OrderSubtotalExclTax { get; set; }
        [JsonProperty("order_sub_total_discount_incl_tax")] public double OrderSubTotalDiscountInclTax { get; set; }
        [JsonProperty("order_sub_total_discount_excl_tax")] public double OrderSubTotalDiscountExclTax { get; set; }
        [JsonProperty("order_shipping_incl_tax")] public double OrderShippingInclTax { get; set; }
        [JsonProperty("order_shipping_excl_tax")] public double OrderShippingExclTax { get; set; }
        [JsonProperty("payment_method_additional_fee_incl_tax")] public double PaymentMethodAdditionalFeeInclTax { get; set; }
        [JsonProperty("payment_method_additional_fee_excl_tax")] public double PaymentMethodAdditionalFeeExclTax { get; set; }
        [JsonProperty("tax_rates")] public string TaxRates { get; set; }
        [JsonProperty("order_tax")] public double OrderTax { get; set; }
        [JsonProperty("order_discount")] public double OrderDiscount { get; set; }
        [JsonProperty("order_total")] public double OrderTotal { get; set; }
        [JsonProperty("refunded_amount")] public double RefundedAmount { get; set; }
        [JsonProperty("checkout_attribute_description")] public string CheckoutAttributeDescription { get; set; }
        [JsonProperty("customer_language_id")] public int CustomerLanguageId { get; set; }
        [JsonProperty("affiliate_id")] public int AffiliateId { get; set; }
        [JsonProperty("customer_ip")] public string CustomerIp { get; set; }
        [JsonProperty("authorization_transaction_id")] public string AuthorizationTransactionId { get; set; }
        [JsonProperty("authorization_transaction_code")] public object AuthorizationTransactionCode { get; set; }
        [JsonProperty("authorization_transaction_result")] public object AuthorizationTransactionResult { get; set; }
        [JsonProperty("capture_transaction_id")] public object CaptureTransactionId { get; set; }
        [JsonProperty("capture_transaction_result")] public object CaptureTransactionResult { get; set; }
        [JsonProperty("subscription_transaction_id")] public object SubscriptionTransactionId { get; set; }
        [JsonProperty("paid_date_utc")] public DateTime? PaidDateUtc { get; set; }
        [JsonProperty("shipping_method")] public string ShippingMethod { get; set; }
        [JsonProperty("shipping_rate_computation_method_system_name")] public string ShippingRateComputationMethodSystemName { get; set; }
        [JsonProperty("custom_values_xml")] public object CustomValuesXml { get; set; }
        [JsonProperty("deleted")] public bool Deleted { get; set; }
        [JsonProperty("created_on_utc")] public DateTime CreatedOnUtc { get; set; }
        [JsonProperty("customer")] public Customer Customer { get; set; }
        [JsonProperty("customer_id")] public int CustomerId { get; set; }
        [JsonProperty("billing_address")] public BillingAddress BillingAddress { get; set; }
        [JsonProperty("shipping_address")] public ShippingAddress ShippingAddress { get; set; }
        [JsonProperty("order_items")] public List<OrderItem> OrderItems { get; set; }
        [JsonProperty("order_status")] public string OrderStatus { get; set; }
        [JsonProperty("payment_status")] public string PaymentStatus { get; set; }
        [JsonProperty("Paid")] public bool Paid { get; set; }
        [JsonProperty("shipping_status")] public string ShippingStatus { get; set; }
        [JsonProperty("customer_tax_display_type")] public string CustomerTaxDisplayType { get; set; }
        [JsonProperty("id")] public int Id { get; set; }

        [JsonProperty("corresponding_account_number")]
        public string CorrespondingAccountNumber { get; set; }

        [JsonProperty("corresponding_account_has_receipt")]
        public bool CorrespondingAccountHasReceipt { get; set; }

        public Order Clone()
        {
            return (Order)MemberwiseClone();
        }
    }

    public class OrderRootObject
    {
        public List<Order> Orders { get; set; }
    }
}
