﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SyncClient.Lib
{
    public class SettingsModel
    {
        public SettingsModel()
        {

        }
        public string NopUsername { get; set; }
        public string NopPassword { get; set; }
        public string NopServerAddress { get; set; }
        public string NopAuthToken { get; set; }
        public string AccUsername { get; set; }
        public string AccPassword { get; set; }
        public string AccServerAddress { get; set; }
        public string StokeId { get; set; }
        public string ShippingMethodId { get; set; }
        public string DefaultCustomerId { get; set; }
        public string SaleTypeId { get; set; }

        private static readonly string SettingPath = $"{Application.StartupPath}/app.xml";
        public static SettingsModel LoadSettings()
        {
            if (!File.Exists(SettingPath))
            {
                return new SettingsModel();
            }
            var file = File.OpenRead(SettingPath);

            XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
            try
            {
                var model = (SettingsModel)serializer.Deserialize(file);
                file.Close();
                return model;

            }
            catch (Exception e)
            {
                return new SettingsModel();
            }
            finally
            {
                file.Close();
            }
        }

        public void SaveSetting()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
            try
            {
                var file = File.OpenWrite($"{SettingPath}.temp");
                serializer.Serialize(file, this);
                file.Close();
                if (File.Exists(SettingPath))
                    File.Delete(SettingPath);
                File.Copy($"{SettingPath}.temp", SettingPath, false);
            }
            catch (Exception exception)
            {
                //Ignore
            }
        }
    }
}
