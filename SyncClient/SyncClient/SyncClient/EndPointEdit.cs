﻿using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SyncClient
{
    public partial class EndPointEdit : TextBox
    {
        private const string EndPointRegex = @"(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'"".,<>?«»“”‘’]))?";
        public EndPointEdit()
        {
            LostFocus += (sender, args) =>
           {
               if (!string.IsNullOrEmpty(Text) && !Regex.IsMatch(Text, EndPointRegex))
               {
                   MessageBox.Show("Invalid end point address");
                   Text = string.Empty;
               }
           };
        }
    }
}
